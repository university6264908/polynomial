package com.polynomial;

import java.util.ArrayList;
import java.util.List;

/**
 * A mathematical polynomial function.
 * 
 * @author Matthew Satti
 * @version 1.0
 */
public class Polynomial {
	/**
	 * Stores the terms in the polynomial.
	 */
	private List<Term> polynomial;
	
	/**
	 * Constructs an empty polynomial.
	 */
	public Polynomial() {
		polynomial = new ArrayList<>();
	}
	
	/**
	 * Adds a term to the polynomial
	 * @param term The term to add.
	 */
	public void addTerm(Term term) {
		polynomial.add(term);
	}
	
	@Override
	public String toString() {
		String output = "";
		for (Term t : polynomial) {
			output += t + " ";
		}
		
		// Clean string
		output = output.trim();
		if (output.startsWith("+")) {
			return output.replaceFirst("\\+ ", "");
		} else if (output.isEmpty()) {
			return "0";
		} else {
			return output;
		}
	}
}
