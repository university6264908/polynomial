package com.polynomial;

import java.lang.Math;
import java.text.DecimalFormat;

/**
 * A single term in a polynomial.
 * 
 * @author Matthew Satti
 * @version 1.0
 */
public class Term {
	/**
	 * The coefficient of the term.
	 */
	private double coefficient;
	
	/**
	 * The exponent of the term.
	 */
	private int exponent;
	
	/**
	 * Constructs a new term.
	 * @param coefficient The coefficient of the term.
	 * @param exponent The exponent of the term.
	 */
	public Term(double coefficient, int exponent) {
		this.coefficient = coefficient;
		this.exponent = exponent;
	}
	
	/**
	 * Gets the coefficient.
	 */
	public double getCoefficient() {
		return coefficient;
	}
	
	/**
	 * Gets the exponent.
	 */
	public double getExponent() {
		return exponent;
	}
	
	@Override
	public String toString() {
		String termAsString = "";
		
		// Determine sign
		int signResult = Double.compare(coefficient, 0);
		if (signResult == 0) {
			return "";
		} else if (signResult > 0) {
			termAsString += "+ ";
		} else {
			termAsString += "- ";
		}
		
		// Determine magnitude
		int absResult = Double.compare(Math.abs(coefficient), 1);
		if (absResult != 0 || (absResult == 0 && exponent == 0)) {
			DecimalFormat df = new DecimalFormat("0.#");
			termAsString += df.format(Math.abs(coefficient));
		}
		
		// Determine exponent
		if (exponent == 0) {
			return termAsString;
		} else if (exponent == 1) {
			termAsString += "x";
		} else {
			termAsString += "x^" + exponent;
		}
		
		return termAsString;
	}
}
