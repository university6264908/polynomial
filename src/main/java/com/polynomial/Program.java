package com.polynomial;

import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		System.out.println("Enter a sequence of numbers (starting x = 0):");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		scanner.close();
		
		Polynomial answer = new Polynomial();
		Sequence sequence = new Sequence(input);
		Sequence currentSequence = sequence;
		Term currentTerm;
		
		do {
			currentTerm = currentSequence.nextTerm();
			answer.addTerm(currentTerm);
			currentSequence = currentSequence.removeTermEffect(currentTerm);
		} while (currentTerm.getExponent() != 0);
	
		System.out.println(answer);
	}
}
