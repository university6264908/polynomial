package com.polynomial;

/**
 * A very basic helper class for math related functions.
 * 
 * @author Matthew Satti
 * @version 1.0
 */
public class MathHelper {
	private static final double DELTA = 0.00000001;
	
	/**
	 * Calculates the factorial of a number.
	 * @param num The number to calculate a factorial for.
	 */
	public static int factorial(int num) {
		int factorial = 1;
		
		for (int i = 2; i <= num; i++) {
			factorial *= i;
		}
		
		return factorial;
	}
	
	public static boolean compareDoubles(double d1, double d2) {
		return Math.abs(d1 - d2) < DELTA;
	}
}
