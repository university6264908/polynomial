package com.polynomial;

import java.lang.Math;

/**
 * Models a sequence of numbers.
 * 
 * @author Matthew Satti
 * @version 1.0
 */
public class Sequence {
	/**
	 * The numbers in the sequence.
	 */
	private double[] sequence;
	
	/**
	 * Constructs a new sequence object.
	 * @param string The sequence as a string.
	 */
	public Sequence(String string) {
		// Split on commas and store
		String[] numbers = string.split(",");
		sequence = new double[numbers.length];
		
		// Parse all numbers
		for (int i = 0; i < numbers.length; i++) {
			sequence[i] = Double.parseDouble(numbers[i]);
		}
	}
	
	/**
	 * Constructs a new sequence object.
	 * @param sequence The sequence as doubles.
	 */
	public Sequence(double[] sequence) {
		this.sequence = new double[sequence.length];
		for (int i = 0; i < sequence.length; i++) {
			this.sequence[i] = sequence[i];
		}
	}
	
	/**
	 * Returns the number in the sequence at the specified index.
	 * @param index The index of the number to return.
	 */
	public double getNumberInSequence(int index) {
		return sequence[index];
	}
	
	/**
	 * Returns the length of the sequence.
	 */
	public int getLength() {
		return sequence.length;
	}
	
	/**
	 * Determines if all numbers in the sequence are equal.
	 */
	public boolean allEqual() {
		for (int i = 1; i < sequence.length; i++) {
			if (!MathHelper.compareDoubles(sequence[0], sequence[i])) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Returns a sequence which is the difference between the numbers.
	 * The returned sequence is one shorter than the original.
	 */
	public Sequence difference() {
		// Test new length
		int length = sequence.length - 1;
		if (length < 2) {
			System.err.println("No pattern could be detected.");
			System.exit(1);
		}
		
		double[] differences = new double[length];
		for (int i = 0; i < length; i++) {
			differences[i] = sequence[i + 1] - sequence[i];
		}
		return new Sequence(differences);
	}
	
	/**
	 * Removes the effect of the term from the sequence.
	 * @param term The term's effect to remove.
	 */
	public Sequence removeTermEffect(Term term) {
		// Copy sequence first
		double[] newSequence = new double[sequence.length];
		for (int i = 0; i < sequence.length; i++) {
			double termEffect = term.getCoefficient() * Math.pow(i, term.getExponent());
			newSequence[i] = sequence[i] - termEffect;
		}
		
		return new Sequence(newSequence);
	}
	
	/**
	 * Returns a term based on the sequence.
	 */
	public Term nextTerm() {
		int iterations = 0;
		Sequence nextSequence = this;
		while (!nextSequence.allEqual()) {
			nextSequence = nextSequence.difference();
			iterations++;
		}
		
		double coefficient = nextSequence.getNumberInSequence(0) / MathHelper.factorial(iterations);
		return new Term(coefficient, iterations);
	}
}
